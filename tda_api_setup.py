from tda import auth, client
import config
from selenium import webdriver

def direct_access():
    api_connection = auth.client_from_token_file(config.td_ameritrade_paper_trading['TOKEN_PATH'],
        config.td_ameritrade_paper_trading['API_KEY'])
    return api_connection

def need_to_relog_in():
    with webdriver.Chrome(executable_path=config.td_ameritrade_paper_trading['CHROMEDRIVER_PATH']) as driver:
        api_connection = auth.client_from_login_flow(
            driver,
            config.td_ameritrade_paper_trading['API_KEY'],
            config.td_ameritrade_paper_trading['REDIRECT_URI'],
            config.td_ameritrade_paper_trading['TOKEN_PATH'])
        return api_connection

def connect_to_api():
    try:
        direct_access()

    except FileNotFoundError:
        need_to_relog_in()
