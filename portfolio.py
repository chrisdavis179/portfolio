import alpaca_trade_api as trade_api
import config
import pandas as pd

connection = trade_api.REST(config.alpaca_paper_trading['KEY_ID'], 
    config.alpaca_paper_trading['SECRET_KEY'],
    config.alpaca_paper_trading['BASE_URL']
    )

def get_account_information():
    response = connection.get_account()._raw
    data = []
    for value in response: 
        data.append([value, response[value]])
    
    df = pd.DataFrame(data)
    return df


print(get_account_information())