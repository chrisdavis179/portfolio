# Trading Portfolio Project

The objective of this project is to build a systematic automated trading system that automates the screening, testing and deploying of strategies. Currently the system will be implemented on a paper trading account through [Alpaca](https://alpaca.markets) with objective to to migrate system to live trading account through [Robinhood](https://robinhood.com) once system is stable.

## Components

### Dev Tools
- alpaca-trade-api (paper); robin-stocks (real)
- backtrader, bta-lib
- pandas, numpy, matplotlib, seaborn

- vollib (considering)
### Screener

### Backtest

### Portfolio