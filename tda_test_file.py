################################################
from tda.orders import equities as equities
from tda.client import Client
import tda
from tda import auth, client
import json
import tda_api_setup as setup
import config
import pandas as pd
from selenium import webdriver

from tda.orders.equities import equity_buy_limit
from tda.orders.common import Duration, Session
################################################

api_connection = auth.client_from_token_file(config.td_ameritrade_paper_trading['TOKEN_PATH'],
    config.td_ameritrade_paper_trading['API_KEY'])

conn = api_connection

paper_acct_id = config.td_ameritrade_paper_trading['REAL_ACCOUNT_ID']
# real_acct_id = config.td_ameritrade_paper_trading['REAL_ACCOUNT_ID']
# output = conn.get_account(config.td_ameritrade_paper_trading['PAPER_TRADING_ID'])

# sample_order = equities.equity_buy_market(['AAPL'], 2)

# execute_order = conn.place_order(acct_id, sample_order
#         .set_duration(Duration.GOOD_TILL_CANCEL)
#         .set_session(Session.SEAMLESS)
#         .build())

execute_order = conn.place_order(
    paper_acct_id,
    equity_buy_limit('GOOG', 1, 1250.0)
        .set_duration(Duration.GOOD_TILL_CANCEL)
        .set_session(Session.SEAMLESS)
        .build())

response = conn.get_account(paper_acct_id)
################################################
'''
Below is to test and display output in console
'''

print()
# print(json.dumps(output.json(), indent=4))
# print(execute_order.json())
print(json.dumps(response.json(), indent=4))
print()
