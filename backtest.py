
import backtrader as bt
import backtrader.analyzers as btanalyzers
import matplotlib
from matplotlib import pyplot as plt
from datetime import datetime
import pandas as pd
 

 
cerebro = bt.Cerebro()

prices=bt.feeds.YahooFinanceData(dataname = 'MSFT', fromdate = datetime(2010, 1, 1), todate = datetime(2020, 1, 1))
prices=prices
cerebro.adddata(prices)
 
cerebro.addstrategy(Hold)

initial_investment = 70000.00 
cerebro.broker.setcash(initial_investment)
 
cerebro.addsizer(bt.sizers.PercentSizer, percents = 10)
 
back = cerebro.run()
 
print('Your initial investment was {}'.format(initial_investment))
print('Your account value is now {}'.format(round(cerebro.broker.getvalue(),2)))
print('Your return is {}%'.format((cerebro.broker.getvalue() - initial_investment) // initial_investment * 100))


# cerebro.plot()